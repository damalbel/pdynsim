from multisim import *

# Note, I should define an external function to create the 2 bus system.
# then the other functions just add their dynamics and so on..

# Two bus system with classical generator and Z load
def test1():
    
    eps = 0.0001

    print "Creating a 2 bus system"
    time  = np.linspace(0,2,100)
    tstep = time[1]

    
    # create system: this can be a file that we parse
    dsystem = System()
    
    for i in range(2):
	dsystem.addbus(i)

    dsystem.buses[0].setvinit(cmath.rect(1.04, 0))
    dsystem.buses[1].setvinit(cmath.rect(1.01613, (np.pi/180.0)*-3.3252))

    dsystem.addbranch(0, 1, 0.0001, 0.0576)
    dsystem.createYbusReal()

    gen = GenCLS(0, 5.0, 1.0)
    gen.setinitpow(1.06496, 0.45996)

    dsystem.adddevice(gen)
    
    load = ZLoad(1, (1.03123 + 1j*-0.37616))
    dsystem.addload(load)


    # Solve system
    x = initialize_system(dsystem)
    print x
    x0 = x
    x = integrate_system(x, dsystem, time)
    print x
    assert (np.linalg.norm(x - x0) - 0.151014573238) < eps, "test failed"

    fault_id = dsystem.applyBusFault(1, 0.001)
    assert (abs(np.linalg.norm(dsystem.ybus) - 1415.10841319)) < eps, "test failed: applyBusFault"
    x = solve_steady_system(x, dsystem)

    time = np.linspace(2,2.2,20)
    x = integrate_system(x, dsystem, time)

    dsystem.removeBusFault(fault_id)
    assert (abs(np.linalg.norm(dsystem.ybus) - 49.1045635799)) < eps, "test failed: applyBusFault"
    x = solve_steady_system(x, dsystem)

    time = np.linspace(2.2,6,200)
    x = integrate_system(x, dsystem, time)

def test2():
    
    eps = 0.0001

    print "Creating a 2 bus system"
    time  = np.linspace(0,2,100)
    tstep = time[1]

    
    # create system: this can be a file that we parse
    dsystem = System()
    
    for i in range(2):
	dsystem.addbus(i)

    dsystem.buses[0].setvinit(cmath.rect(1.04, 0))
    dsystem.buses[1].setvinit(cmath.rect(1.01613, (np.pi/180.0)*-3.3252))

    dsystem.addbranch(0, 1, 0.0001, 0.0576)
    dsystem.createYbusReal()

    gen1 = GenCLS(0, 5.0, 1.0)
    gen1.setinitpow(1.06496/2.0, 0.45996/2.0)
    gen2 = GenCLS(0, 5.0, 1.0)
    gen2.setinitpow(1.06496/2.0, 0.45996/2.0)

    dsystem.adddevice(gen1)
    dsystem.adddevice(gen2)
    
    load = ZLoad(1, (1.03123 + 1j*-0.37616))
    dsystem.addload(load)

    # Solve system
    x = initialize_system(dsystem)
    print x
    x0 = x
    x = integrate_system(x, dsystem, time)
    print x
    assert (np.linalg.norm(x - x0) - 0.151014573238) < eps, "test failed"

    fault_id = dsystem.applyBusFault(1, 0.001)
    assert (abs(np.linalg.norm(dsystem.ybus) - 1415.10841319)) < eps, "test failed: applyBusFault"
    x = solve_steady_system(x, dsystem)

    time = np.linspace(2,2.2,20)
    x = integrate_system(x, dsystem, time)

    dsystem.removeBusFault(fault_id)
    assert (abs(np.linalg.norm(dsystem.ybus) - 49.1045635799)) < eps, "test failed: applyBusFault"
    x = solve_steady_system(x, dsystem)

    time = np.linspace(2.2,6,200)
    x = integrate_system(x, dsystem, time)

def test_GENROU():
    
    eps = 0.0001

    print "Creating a 2 bus system"
    time  = np.linspace(0,2,500)
    tstep = time[1]

    
    # create system: this can be a file that we parse
    dsystem = System()
    
    for i in range(2):
	dsystem.addbus(i)

    dsystem.buses[0].setvinit(cmath.rect(1.04, 0))
    dsystem.buses[1].setvinit(cmath.rect(1.01613, (np.pi/180.0)*-3.3252))

    dsystem.addbranch(0, 1, 0.0001, 0.0576)
    dsystem.createYbusReal()

    gen = GenGENROU(0, 1.575, 1.512, 0.29, 0.39, 0.1733, 0.0787,
            3.38, 0.0, 6.09, 1.0, 0.05, 0.15)
    gen.setinitpow(1.06496, 0.45996)

    dsystem.adddevice(gen)
    
    load = ZLoad(1, (1.03123 + 1j*-0.37616))
    dsystem.addload(load)
    dsystem.verbose = True

    # Solve system
    x = initialize_system(dsystem)

    x0 = x
    x = integrate_system(x, dsystem, time)

    fault_id = dsystem.applyBusFault(1, 0.001)
    x = solve_steady_system(x, dsystem)

    time = np.linspace(2,2.2,100)
    x = integrate_system(x, dsystem, time)

    dsystem.removeBusFault(fault_id)
    x = solve_steady_system(x, dsystem)

    time = np.linspace(2.2,6,1000)
    x = integrate_system(x, dsystem, time)

def test4():
    
    eps = 0.0001

    print "Creating a 2 bus system"
    time  = np.linspace(0,2,500)
    tstep = time[1]

    
    # create system: this can be a file that we parse
    dsystem = System()
    
    for i in range(2):
	dsystem.addbus(i)

    dsystem.buses[0].setvinit(cmath.rect(1.04, 0))
    dsystem.buses[1].setvinit(cmath.rect(1.01613, (np.pi/180.0)*-3.3252))

    dsystem.addbranch(0, 1, 0.0001, 0.0576)
    dsystem.createYbusReal()

    gen = GenGENROU(0, 1.575, 1.512, 0.29, 0.39, 0.1733, 0.0787,
            3.38, 0.0, 6.09, 1.0, 0.05, 0.15)
    gen.setinitpow(1.06496, 0.45996)

    dsystem.adddevice(gen)
    
    load = ZLoad(1, (1.03123 + 1j*-0.37616))
    dsystem.addload(load)
    dsystem.verbose = True

    # Solve system
    x = initialize_system(dsystem)
    print x

    exit()

    x0 = x
    x = integrate_system(x, dsystem, time)

    fault_id = dsystem.applyBusFault(1, 0.001)
    x = solve_steady_system(x, dsystem)

    time = np.linspace(2,2.2,100)
    x = integrate_system(x, dsystem, time)

    dsystem.removeBusFault(fault_id)
    x = solve_steady_system(x, dsystem)

    time = np.linspace(2.2,6,1000)
    x = integrate_system(x, dsystem, time)



if  __name__ == "__main__":
    
    import argparse
    
    parser = argparse.ArgumentParser()

    parser.add_argument('-test1', action='store_true', default=False,
        dest='test1', help='Execute test 1: simple 2 bus system, 1 GENCLS')
    parser.add_argument('-test2', action='store_true', default=False,
        dest='test2', help='Execute test 2: simple 2 bus system, 2 GENCLS')
    parser.add_argument('-test3', action='store_true', default=False,
        dest='test3', help='Execute test 3: simple 2 bus system, 1 GENROU')
    parser.add_argument('-test4', action='store_true', default=False,
        dest='test4', help='Execute test 4: simple 2 bus system, 1 GENROU, 1 EXC')

    results = parser.parse_args()
    
    if results.test1:
        test1()

    if results.test2:
        test2()

    if results.test3:
        test_GENROU()
    
    if results.test4:
        test4()
