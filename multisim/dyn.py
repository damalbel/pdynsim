class DynamicModel(object):
    """
    Base class for dynamic model object.
    """

    def __init__(self,ddim,adim,bus,model_type):
        self.dif_dim    = ddim 
        self.alg_dim    = adim
        self.bus        = bus
        self.model_type = model_type

    def getdim(self):
        return self.dif_dim, self.alg_dim

    def setpointers(self, dif_ptr, alg_ptr, ndev):
        self.dif_ptr = dif_ptr
        self.alg_ptr = alg_ptr
        self.ndev    = ndev #device number

    def __str__(self):
        return ("Dynamic object id: %d\n" % (self.id_model) +
         "\tAlgebraic dof: %d\tGlobal Pointer: %d\n" % (self.alg_dim, self.alg_ptr) +
         "\tDifferential dof: %d\tGlobal Pointer: %d" % (self.dif_dim, self.dif_ptr))
