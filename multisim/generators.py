from dyn import DynamicModel
import numpy as np
import cmath
from numpy.linalg import inv, det
from scipy import optimize

class Generator(DynamicModel):

    def __init__(self, bus, initdim, ddim, adim, state_list):
        self.initdim = initdim
        self.state_list = state_list
        DynamicModel.__init__(self,ddim,adim,bus,'generator')
        self.initialized = False

    def setinitpow(self, p0, q0):
        # set initial power, from power flow solution
        self.p0 = p0
        self.q0 = q0

class GenCLS(Generator):

    def __init__(self, n, H, xd):
        self.H  = H
        self.xd = xd
	# initial dimension, dynamic dimension, algebraic dymension
        state_list = ['delta','w', 'v_q', 'v_d', 'i_q', 'i_d']
        Generator.__init__(self, n, 8, 2, 4, state_list)

    def display(self):
        print "Generator at bus %d" % (self.n)

    def residualFinit(self, x, v, theta, p0, q0):

        F    = np.zeros(self.initdim)
        delta = x[0]
        w     = x[1]
        v_q   = x[2]
        v_d   = x[3]
        i_q   = x[4]
        i_d   = x[5]
        e_q   = x[6]
        p_m   = x[7]

        F[0] = 2.0*np.pi*60.0*w
        F[1] = p_m - (v_q*i_q + v_d*i_d)
        F[2] = v_q - e_q + self.xd*i_d
        F[3] = v_d - self.xd*i_q
        F[4] = v*np.sin(delta - theta) - v_d
        F[5] = v*np.cos(delta - theta) - v_q
        F[6] = v_d*i_d + v_q*i_q - p0
        F[7] = v_q*i_d - v_d*i_q - q0

        return F

    def initialize(self, vr, vi, x, y):

        v = np.abs(vr + 1j*vi)
        theta = cmath.phase(vr + 1j*vi)
        
        x0  = np.ones(self.initdim)
        sol = optimize.root(self.residualFinit, x0, args = (v, theta, self.p0, self.q0), 
              method = 'krylov',options={'xtol': 1e-8, 'disp': False})

        self.e_q  = sol.x[6]
        self.p_m  = sol.x[7]

	print self.e_q, self.p_m
        self.initialized = True
        x[self.dif_ptr:self.dif_ptr + 2] = sol.x[0:2]
        y[self.alg_ptr:self.alg_ptr + 4] = sol.x[2:6]

        return None
      
    def residualDiff(self, x_dif, x_alg, f):
        """
        Name: residualDiff
        Description: computes the residual vector for the differential part

        OUTPUTS:
        1. np.array residual differential vector
        """

        delta = x_dif[self.dif_ptr]
        w     = x_dif[self.dif_ptr + 1]

        v_q   = x_alg[self.alg_ptr]
        v_d   = x_alg[self.alg_ptr + 1]
        i_q   = x_alg[self.alg_ptr + 2]
        i_d   = x_alg[self.alg_ptr + 3]

        f[self.dif_ptr]     = 2.0*np.pi*60.0*w
        f[self.dif_ptr + 1] = (self.p_m - (v_q*i_q + v_d*i_d))/(2*self.H)
        
        return None
    
    def residualAlg(self, x_dif, x_alg, vt, g):
        """
        Name: residualAlg
        Description: computes the residual vector for the algebraic part

        OUTPUTS:
        1. np.array residual vector g
        """
        delta = x_dif[self.dif_ptr]
        w     = x_dif[self.dif_ptr + 1]

        v_q   = x_alg[self.alg_ptr]
        v_d   = x_alg[self.alg_ptr + 1]
        i_q   = x_alg[self.alg_ptr + 2]
        i_d   = x_alg[self.alg_ptr + 3]
        
        v = np.abs(vt[0] + 1j*vt[1])
        theta = cmath.phase(vt[0] + 1j*vt[1])


        g[self.alg_ptr]     = v_q - self.e_q + self.xd*i_d
        g[self.alg_ptr + 1] = v_d - self.xd*i_q
        g[self.alg_ptr + 2] = v*np.sin(delta - theta) - v_d
        g[self.alg_ptr + 3] = v*np.cos(delta - theta) - v_q

        return None
        
    def injectedCurrents(self, x_dif, x_alg, curr):
        """
        OUTPUTS:
        1. np.array residual vector f and g
        """
        delta = x_dif[self.dif_ptr]
        w     = x_dif[self.dif_ptr + 1]

        v_q   = x_alg[self.alg_ptr]
        v_d   = x_alg[self.alg_ptr + 1]
        i_q   = x_alg[self.alg_ptr + 2]
        i_d   = x_alg[self.alg_ptr + 3]

        curr[2*self.bus    ] += np.sin(delta)*i_d + np.cos(delta)*i_q
        curr[2*self.bus + 1] += -np.cos(delta)*i_d + np.sin(delta)*i_q



# implementation of GENROU
class GenGENROU(Generator):

    def __init__(self, n,x_d, x_q, x_dp, x_qp, x_ddp,
        xl, H, D, T_d0p, T_q0p, T_d0dp, T_q0dp):

        self.x_d      = x_d
        self.x_q      = x_q
        self.x_dp     = x_dp
        self.x_qp     = x_qp
        self.x_ddp    = x_ddp
        self.x_qdp    = x_ddp
        self.xl       = xl
        self.H        = H
        self.D        = D
        self.T_d0p    = T_d0p
        self.T_q0p    = T_q0p
        self.T_d0dp   = T_d0dp
        self.T_q0dp   = T_q0dp
        
        state_list = ['e_qp', 'e_dp', 'phi_1d', 'phi_2q', 'w','delta', \
                'v_q', 'v_d', 'i_q', 'i_d']

	# initial dimension, dynamic dimension, algebraic dimension
        Generator.__init__(self, n, 12, 6, 4, state_list)

    def display(self):
        print "Generator at bus %d" % (self.n)

    def residualFinit(self, x, v, theta, p0, q0):

        F        = np.zeros(self.initdim)
        
	# state variables
	e_qp     = x[0]
        e_dp     = x[1]
        phi_1d   = x[2]
        phi_2q   = x[3]
        w 	 = x[4]
        delta    = x[5]
        v_q   	 = x[6]
        v_d      = x[7]
        i_q      = x[8]
        i_d      = x[9]
        e_fd     = x[10]
        p_m      = x[11]
	
	# parameters
        x_d      = self.x_d
        x_q      = self.x_q
        x_dp     = self.x_dp
        x_qp     = self.x_qp
        x_ddp    = self.x_ddp
        x_qdp    = self.x_ddp
        xl       = self.xl
        H        = self.H
        D        = self.D
        T_d0p    = self.T_d0p
        T_q0p    = self.T_q0p
        T_d0dp   = self.T_d0dp
        T_q0dp   = self.T_q0dp

	# auxiliary variables
  	psi_de = (x_ddp - xl)/(x_dp - xl)*e_qp + \
		(x_dp - x_ddp)/(x_dp - xl)*phi_1d
  	
	psi_qe = -(x_ddp - xl)/(x_qp - xl)*e_dp + \
		(x_qp - x_ddp)/(x_qp - xl)*phi_2q

	# Machine states
	F[0] = (-e_qp + e_fd - (i_d - (-x_ddp + x_dp)*(-e_qp + i_d*(x_dp - xl) \
		+ phi_1d)/((x_dp - xl)**2.0))*(x_d - x_dp))/T_d0p
    	F[1] = (-e_dp + (i_q - (-x_qdp + x_qp)*( e_dp + i_q*(x_qp - xl) \
		+ phi_2q)/((x_qp - xl)**2.0))*(x_q - x_qp))/T_q0p
    	F[2] = ( e_qp - i_d*(x_dp - xl) - phi_1d)/T_d0dp
    	F[3] = (-e_dp - i_q*(x_qp - xl) - phi_2q)/T_q0dp
    	F[4] = (p_m - psi_de*i_q + psi_qe*i_d)/(2.0*H)
    	F[5] = 2.0*np.pi*60.0*w
	
	# Stator currents
	F[6] = i_d - ((x_ddp - xl)/(x_dp - xl)*e_qp + \
		(x_dp - x_ddp)/(x_dp - xl)*phi_1d - v_q)/x_ddp
	F[7] = i_q - (-(x_qdp - xl)/(x_qp - xl)*e_dp + \
		(x_qp - x_qdp)/(x_qp - xl)*phi_2q + v_d)/x_qdp
	
	# Stator voltage
        F[8] = v_d - v*np.sin(delta - theta)
        F[9] = v_q - v*np.cos(delta - theta)

	#Stator additional equations
        F[10] = v_d*i_d + v_q*i_q - p0
        F[11] = v_q*i_d - v_d*i_q - q0
        
        # Store currents for filtering
        self.i_re = np.sin(delta)*i_d + np.cos(delta)*i_q
        self.i_im = -np.cos(delta)*i_d + np.sin(delta)*i_q

        return F

    def initialize(self, vr, vi, x, y):

        v = np.abs(vr + 1j*vi)
        theta = cmath.phase(vr + 1j*vi)
        
        x0  = np.ones(self.initdim)
        sol = optimize.root(self.residualFinit, x0, args = (v, theta, self.p0, self.q0), 
              method = 'krylov',options={'xtol': 1e-8, 'disp': False})

        self.e_fd = sol.x[10]
        self.p_m  = sol.x[11]
        self.initialized = True
        x[self.dif_ptr:self.dif_ptr + 6] = sol.x[0:6]
        y[self.alg_ptr:self.alg_ptr + 4] = sol.x[6:10]

        return None
      
    def residualDiff(self, x_dif, x_alg, f):
        """
        Name: residualDiff
        Description: computes the residual vector for the differential part

        OUTPUTS:
        1. np.array residual differential vector
        """

	# state variables
	e_qp     = x_dif[self.dif_ptr]
        e_dp     = x_dif[self.dif_ptr + 1]
        phi_1d   = x_dif[self.dif_ptr + 2]
        phi_2q   = x_dif[self.dif_ptr + 3]
        w 	 = x_dif[self.dif_ptr + 4]
        delta    = x_dif[self.dif_ptr + 5]
        
        # algebraic variables
        v_q   	 = x_alg[self.alg_ptr]
        v_d      = x_alg[self.alg_ptr + 1]
        i_q      = x_alg[self.alg_ptr + 2]
        i_d      = x_alg[self.alg_ptr + 3]
	
	# parameters
        x_d      = self.x_d
        x_q      = self.x_q
        x_dp     = self.x_dp
        x_qp     = self.x_qp
        x_ddp    = self.x_ddp
        x_qdp    = self.x_ddp
        xl       = self.xl
        H        = self.H
        D        = self.D
        T_d0p    = self.T_d0p
        T_q0p    = self.T_q0p
        T_d0dp   = self.T_d0dp
        T_q0dp   = self.T_q0dp

        # control variables
        e_fd    = self.e_fd
        p_m     = self.p_m
	
        # auxiliary variables
  	psi_de = (x_ddp - xl)/(x_dp - xl)*e_qp + \
		(x_dp - x_ddp)/(x_dp - xl)*phi_1d
  	
	psi_qe = -(x_ddp - xl)/(x_qp - xl)*e_dp + \
		(x_qp - x_ddp)/(x_qp - xl)*phi_2q

	# Machine states
	f[self.dif_ptr + 0] = (-e_qp + e_fd - (i_d - (-x_ddp + x_dp)*(-e_qp + i_d*(x_dp - xl) \
		+ phi_1d)/((x_dp - xl)**2.0))*(x_d - x_dp))/T_d0p
    	f[self.dif_ptr + 1] = (-e_dp + (i_q - (-x_qdp + x_qp)*( e_dp + i_q*(x_qp - xl) \
		+ phi_2q)/((x_qp - xl)**2.0))*(x_q - x_qp))/T_q0p
    	f[self.dif_ptr + 2] = ( e_qp - i_d*(x_dp - xl) - phi_1d)/T_d0dp
    	f[self.dif_ptr + 3] = (-e_dp - i_q*(x_qp - xl) - phi_2q)/T_q0dp
    	f[self.dif_ptr + 4] = (p_m - psi_de*i_q + psi_qe*i_d)/(2.0*H)
    	f[self.dif_ptr + 5] = 2.0*np.pi*60.0*w

        
        return None
    
    def residualAlg(self, x_dif, x_alg, vt, g):
        """
        Name: residualAlg
        Description: computes the residual vector for the algebraic part

        OUTPUTS:
        1. np.array residual vector g
        """
	# state variables
	e_qp     = x_dif[self.dif_ptr]
        e_dp     = x_dif[self.dif_ptr + 1]
        phi_1d   = x_dif[self.dif_ptr + 2]
        phi_2q   = x_dif[self.dif_ptr + 3]
        w 	 = x_dif[self.dif_ptr + 4]
        delta    = x_dif[self.dif_ptr + 5]
        
        # algebraic variables
        v_q   	 = x_alg[self.alg_ptr]
        v_d      = x_alg[self.alg_ptr + 1]
        i_q      = x_alg[self.alg_ptr + 2]
        i_d      = x_alg[self.alg_ptr + 3]
	
	# parameters
        x_d      = self.x_d
        x_q      = self.x_q
        x_dp     = self.x_dp
        x_qp     = self.x_qp
        x_ddp    = self.x_ddp
        x_qdp    = self.x_ddp
        xl       = self.xl
        H        = self.H
        D        = self.D
        T_d0p    = self.T_d0p
        T_q0p    = self.T_q0p
        T_d0dp   = self.T_d0dp
        T_q0dp   = self.T_q0dp

        # control variables
        e_fd    = self.e_fd
        p_m     = self.p_m

        v = np.abs(vt[0] + 1j*vt[1])
        theta = cmath.phase(vt[0] + 1j*vt[1])

	# Stator currents
	g[self.alg_ptr]     = i_d - ((x_ddp - xl)/(x_dp - xl)*e_qp + \
		(x_dp - x_ddp)/(x_dp - xl)*phi_1d - v_q)/x_ddp
	g[self.alg_ptr + 1] = i_q - (-(x_qdp - xl)/(x_qp - xl)*e_dp + \
		(x_qp - x_qdp)/(x_qp - xl)*phi_2q + v_d)/x_qdp
	
	# Stator voltage
        g[self.alg_ptr + 2] = v_d - v*np.sin(delta - theta)
        g[self.alg_ptr + 3] = v_q - v*np.cos(delta - theta)

        return None
        
    def injectedCurrents(self, x_dif, x_alg, curr):
        """
        OUTPUTS:
        1. np.array residual vector f and g
        """
        delta = x_dif[self.dif_ptr + 5]

        v_q   = x_alg[self.alg_ptr]
        v_d   = x_alg[self.alg_ptr + 1]
        i_q   = x_alg[self.alg_ptr + 2]
        i_d   = x_alg[self.alg_ptr + 3]

        curr[2*self.bus    ] += np.sin(delta)*i_d + np.cos(delta)*i_q
        curr[2*self.bus + 1] += -np.cos(delta)*i_d + np.sin(delta)*i_q

        # Store currents for filtering
        self.i_re = np.sin(delta)*i_d + np.cos(delta)*i_q
        self.i_im = -np.cos(delta)*i_d + np.sin(delta)*i_q
