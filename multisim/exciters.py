from dyn import DynamicModel
import numpy as np
import cmath
from numpy.linalg import inv, det
from scipy import optimize

class Exciter(DynamicModel):

    def __init__(self, bus, initdim, ddim, adim, state_list):
        self.initdim = initdim
        self.state_list = state_list
        DynamicModel.__init__(self,ddim,adim,bus,'exciter')
        self.initialized = False

    def setinitpow(self, p0, q0):
        # set initial power, from power flow solution
        self.p0 = p0
        self.q0 = q0

class ESDC1(Generator):

    def __init__(self, ka, ke, kf, ta, tf, te, tr, v_max, v_min):
	
	self.ka = ka
	self.ke = ke
	self.kf = kf
	self.ta = ta
	self.tf = tf
	self.te = te
	self.tr = tr
	self.v_max = v_max
	self.v_min = v_min
        
        # initial dimension, dynamic dimension, algebraic dymension
        state_list = ['v_r1', 'v_r2', 'e_fd']
        Generator.__init__(self, n, 3, 3, 0, state_list)

    def display(self):
        print "Exciter at bus  %d" % (self.n)

    def residualFinit(self, x, v, theta, p0, q0a):

        F    = np.zeros(self.initdim)
        delta = x[0]
        w     = x[1]
        v_q   = x[2]
        v_d   = x[3]
        i_q   = x[4]
        i_d   = x[5]
        e_q   = x[6]
        p_m   = x[7]

        F[0] = 2.0*np.pi*60.0*w
        F[1] = p_m - (v_q*i_q + v_d*i_d)
        F[2] = v_q - e_q + self.xd*i_d
        F[3] = v_d - self.xd*i_q
        F[4] = v*np.sin(delta - theta) - v_d
        F[5] = v*np.cos(delta - theta) - v_q
        F[6] = v_d*i_d + v_q*i_q - p0
        F[7] = v_q*i_d - v_d*i_q - q0

        return F

    def initialize(self, vr, vi, x, y):

        v = np.abs(vr + 1j*vi)
        theta = cmath.phase(vr + 1j*vi)
        
        x0  = np.ones(self.initdim)
        sol = optimize.root(self.residualFinit, x0, args = (v, theta, self.p0, self.q0), 
              method = 'krylov',options={'xtol': 1e-8, 'disp': False})

        self.e_q  = sol.x[6]
        self.p_m  = sol.x[7]
        self.initialized = True
        x[self.dif_ptr:self.dif_ptr + 2] = sol.x[0:2]
        y[self.alg_ptr:self.alg_ptr + 4] = sol.x[2:6]

        return None
      
    def residualDiff(self, x_dif, x_alg, f):
        """
        Name: residualDiff
        Description: computes the residual vector for the differential part

        OUTPUTS:
        1. np.array residual differential vector
        """

        delta = x_dif[self.dif_ptr]
        w     = x_dif[self.dif_ptr + 1]

        v_q   = x_alg[self.alg_ptr]
        v_d   = x_alg[self.alg_ptr + 1]
        i_q   = x_alg[self.alg_ptr + 2]
        i_d   = x_alg[self.alg_ptr + 3]

        f[self.dif_ptr]     = 2.0*np.pi*60.0*w
        f[self.dif_ptr + 1] = self.p_m - (v_q*i_q + v_d*i_d)
        
        return None
    
    def residualAlg(self, x_dif, x_alg, vt, g):
        """
        Name: residualAlg
        Description: computes the residual vector for the algebraic part

        OUTPUTS:
        1. np.array residual vector g
        """
        delta = x_dif[self.dif_ptr]
        w     = x_dif[self.dif_ptr + 1]

        v_q   = x_alg[self.alg_ptr]
        v_d   = x_alg[self.alg_ptr + 1]
        i_q   = x_alg[self.alg_ptr + 2]
        i_d   = x_alg[self.alg_ptr + 3]
        
        v = np.abs(vt[0] + 1j*vt[1])
        theta = cmath.phase(vt[0] + 1j*vt[1])


        g[self.alg_ptr]     = v_q - self.e_q + self.xd*i_d
        g[self.alg_ptr + 1] = v_d - self.xd*i_q
        g[self.alg_ptr + 2] = v*np.sin(delta - theta) - v_d
        g[self.alg_ptr + 3] = v*np.cos(delta - theta) - v_q

        return None
        
